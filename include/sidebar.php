 <!-- Side-Nav -->
 <div class="side-navbar active-nav border-end border-secondary" id="sidebar">
      <div class="misc_logo border-bottom border-secondary p-3">
          <a href="home.php">
            <img src="assets/images/logo-re.png" class="img-fluid" alt="logo">
          </a>
      </div>
      <div class="px-4 py-4">
        <span class="text-capitalize text-white">Bullseye Lottery Home</span>
      </div>
    
    <ul class="nav flex-column me-3">


    <div class="px-4 py-4">
      <span class="text-capitalize text-white">lottery details</span>
    </div>


      <li class="active_side one"> 
        <img src="assets/images/LR.png" class="img-fluid me-2" alt="result"><a href="powerball.php" class="text-white text-capitalize">latest results</a> 
      </li>
      <li class="one"> 
         <img src="assets/images/LT.png" class="img-fluid me-2" alt="info"> <a href="powerball.php" class="text-white text-capitalize">lottery info</a> 
      </li>
      <li class="one"> 
         <img src="assets/images/stats.png" class="img-fluid me-2" alt="info"> <a href="powerball.php" class="text-white text-capitalize">lottery news</a> 
      </li>
      <li class="one"> 
         <img src="assets/images/stats.png" class="img-fluid me-2" alt="info"> <a href="powerball.php" class="text-white text-capitalize">Shared user comments/discussions</a> 
      </li>
      <li class="one"> 
         <img src="assets/images/stats.png" class="img-fluid me-2" alt="info"> <a href="newsdetails.php" class="text-white text-capitalize">Draw History</a> 
      </li>
      <li class="one"> 
         <img src="assets/images/stats.png" class="img-fluid me-2" alt="info"> <a href="my-account.php" class="text-white text-capitalize">Charts/Graphs</a> 
      </li>
      <li class="one"> 
         <img src="assets/images/stats.png" class="img-fluid me-2" alt="info"> <a href="newsdetails.php" class="text-white text-capitalize">User Draw History</a> 
      </li>
      
      
   
    <div class="px-4 py-4">
      <span class="text-capitalize text-white">generate picks</span>
    </div>

    <li class="one"> 
       <img src="assets/images/PB.png" class="img-fluid me-2" alt="info"> <a href="affiliate.php" class="text-white text-capitalize">BullyEye Auto Picks</a> 
    </li>
    <li class="one"> 
       <img src="assets/images/EJ.png" class="img-fluid me-2" alt="info"> <a href="powerball.php" class="text-white text-capitalize">Custom Picking</a> 
    </li>
    <li class="one"> 
       <img src="assets/images/MM.png" class="img-fluid me-2" alt="info"> <a href="powerball.php" class="text-white text-capitalize">Wheeling Pickers</a> 
    </li>
    <li class="one"> 
       <img src="assets/images/UKL.png" class="img-fluid me-2" alt="info"> <a href="home.php" class="text-white text-capitalize">User Custom Picking Logic</a> 
    </li>
    <!-- <li class="one"> 
       <img src="assets/images/EM.png" class="img-fluid me-2" alt="info"> <a href="#/" class="text-white text-capitalize">euro millions</a> 
    </li>
    <li class="one"> 
      <img src="assets/images/LAUS.png" class="img-fluid me-2" alt="info"> <a href="#/" class="text-white text-capitalize">lotto 6aus49</a> 
   </li> -->



   <div class="px-4 py-4">
      <span class="text-capitalize text-white">about</span>
    </div>
    <li class="one"> 
         <img src="assets/images/TC.png" class="img-fluid me-2" alt="info"> <a href="aboutus.php" class="text-white text-capitalize">terms & condition</a> 
      </li>
   
      <li class="one"> 
         <img src="assets/images/PP.png" class="img-fluid me-2" alt="info"> <a href="aboutus.php" class="text-white text-capitalize">privacy & policy</a> 
      </li>
      <li class="one"> 
         <img src="assets/images/HI.png" class="img-fluid me-2" alt="info"> <a href="aboutus.php" class="text-white text-capitalize">contact us</a> 
      </li>
      <li class="one"> 
         <img src="assets/images/HI.png" class="img-fluid me-2" alt="info"> <a href="aboutus.php" class="text-white text-capitalize">help</a> 
      </li>

  </ul>
  </div>