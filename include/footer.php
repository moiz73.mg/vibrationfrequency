<div class="foter_bg">
      <footer>
        <div class="d-flex flex-column flex-md-row justify-content-between px-5 py-3 align-items-md-center">
          <span class="text_lgray">Copyright © <a href="#/" class="text-danger text-decoration-none">Misco</a>
            2020. All rights reserved.
          </span>
          <img src="assets/images/pay.png" class="img-fluid" alt="payment">
        </div>
      </footer>
    </div>