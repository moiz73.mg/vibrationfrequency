<!DOCTYPE html>
<html lang="en">

<head>
  <?php include 'include/top_links.php'; ?>
</head>

<body>
<div class="main_bg">
<div class="container">
  <div class="row vh-100 justify-content-center align-items-center">
<div class="col-12 col-md-6 col-lg-6">
    <div class="login_section px-3 py-2">
      <form action="#/">
        <div class="text-center mb-5 hard_vib">
            <h1>Hardware  <br> vibration </h1>
        </div>
        <div class="sign_in">Sign up</div>
<div class="row">
        <div class="form-group col-6 mb-3">
            <fieldset class="the-fieldset rounded-3">
                <legend class="the-legend float-none">Full Name</legend>
                <input type="text" placeholder="John" class="form-control text-white p-0 ps-1 bg-transparent border-0">
            </fieldset>
        </div>
        <div class="form-group col-6 mb-3">
            <fieldset class="the-fieldset rounded-3">
                <legend class="the-legend float-none">Username</legend>
                <input type="text" placeholder="John32" class="form-control text-white p-0 ps-1 bg-transparent border-0">
            </fieldset>
        </div>
        <div class="form-group col-6">
          <fieldset class="the-fieldset rounded-3">
              <legend class="the-legend float-none">Email</legend>
              <input type="text" placeholder="John@gmail.com" class="form-control text-white p-0 ps-1 bg-transparent border-0">
          </fieldset>
      </div>
      <div class="form-group col-6">
          <fieldset class="the-fieldset rounded-3">
              <legend class="the-legend float-none">Password</legend>
              <input type="password" placeholder="********" class="form-control text-white p-0 ps-1 bg-transparent border-0">
          </fieldset>
      </div>
        </div>


        <div class="sub text-center col-7 mt-4 mb-2 mx-auto">
            <button class="btn btn_signsubmit">sign in</button>
            <span class="color_white_font">Already have an account? <a href="index.php" class="text-white">SIGN IN</a> </span>
        </div>

      </form>
    </div>
    </div>
  </div>
</div>
</div>



  <?php include 'include/bottom_links.php'; ?>
</body>

</html>