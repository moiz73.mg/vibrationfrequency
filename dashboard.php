<!DOCTYPE html>
<html lang="en">

<head>
    <?php include 'include/top_links.php'; ?>
</head>

<body class="dash_body">

    <div class="header_content">
        <div class="container-fluid container-xxl">
            <div class="d-flex align-items-center pt-3">
                <div class="hard_vib">
                    <h1 class="text-capitalize">Hardware-vibration</h1>
                </div>
                <div class="dropdown_user ms-auto">
                    <div class="dropdown">
                        <a class="dropdown-toggle u_droptoggle text-white" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                            <img src="assets/images/profilePicMask.png" alt="userprofile" class="rounded-circle img-fluid">
                            <span class="u_name">Elizabetg Kin</span>
                        </a>

                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <li><a class="dropdown-item" href="dashboard.php">Dashboard</a></li>
                            <li><a class="dropdown-item" href="users.php">Users</a></li>
                            <li><a class="dropdown-item" href="analytics.php">Analytics</a></li>
                            <li><a class="dropdown-item" href="index.php">Logout</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="butns d-flex flex-wrap align-items-center">
                <div class="dash_btn">
                    <button class="btn btn_site_select">Dashboard</button>
                </div>
                <div class="ms-auto all_selects d-flex">
                    <select class="form-select btn_site_select border-0 form-select-sm" aria-label=".form-select-sm example">
                        <option selected>Amplitude</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>

                    <select class="form-select mx-2 btn_site_select border-0 form-select-sm" aria-label=".form-select-sm example">
                        <option selected>Frequency</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>

                    <select class="form-select btn_site_select border-0 form-select-sm" aria-label=".form-select-sm example">
                        <option selected>Time</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="mt-5">
        <div class="card-group">
            <div class="card text-white bg-secondary m-4" style="max-width: 18rem;cursor:pointer;" onclick="window.location= './dashboard.php'">
                <div class="card-body text-center">
                    <i class="fa-solid fa-table-columns fa-9x"></i>
                    <h5 class="card-title">Dashboard</h5>
                </div>
            </div>
            <div class="card text-white bg-dark m-4" style="max-width: 18rem;cursor:pointer;" onclick="window.location= './analytics.php'">
                <div class="card-body text-center">
                    <i class="fa-solid fa-chart-simple fa-9x"></i>
                    <h5 class="card-title">Analytics</h5>
                </div>
            </div>
            <div class="card text-white bg-secondary m-4" style="max-width: 18rem;cursor:pointer;" onclick="window.location= './users.php'">
                <div class="card-body text-center">
                    <i class="fa-solid fa-user fa-9x"></i>
                    <h5 class="card-title">Users</h5>
                </div>
            </div>
            <div class="card text-white bg-danger m-4" style="max-width: 18rem;cursor:pointer;" onclick="window.location= './index.php'">
                <div class="card-body text-center">
                    <i class="fa fa-power-off fa-9x" aria-hidden="true"></i>
                    <!-- <img class="card-img-top" src="./assets/images/Amplitude.png" alt="Card image cap"> -->
                    <h5 class="card-title">Logout</h5>
                </div>
            </div>
        </div>
    </div>








    <?php include 'include/bottom_links.php'; ?>
</body>

</html>