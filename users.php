<!DOCTYPE html>
<html lang="en">

<head>
  <?php include 'include/top_links.php'; ?>
</head>

<body class="dash_body">
  <div class="header_content">
    <div class="container-fluid container-xxl">
      <div class="d-flex align-items-center pt-3">
        <div class="hard_vib">
          <i class="fa-solid fa-left-long text-white" onclick="window.location='./dashboard.php'"></i>
          <h1 class="text-capitalize">Hardware-vibration</h1>
        </div>
        <div class="dropdown_user ms-auto">
          <div class="dropdown">
            <a class="dropdown-toggle u_droptoggle text-white" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
              <img src="assets/images/profilePicMask.png" alt="userprofile" class="rounded-circle img-fluid">
              <span class="u_name">Elizabetg Kin</span>
            </a>

            <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
              <li><a class="dropdown-item" href="dashboard.php">Dashboard</a></li>
              <li><a class="dropdown-item" href="statistics.php">Statistics</a></li>
              <li><a class="dropdown-item" href="index.php">Logout</a></li>
            </ul>
          </div>
        </div>
      </div>

      <div class="butns d-flex flex-wrap align-items-center">
        <div class="dash_btn">
          <button class="btn btn_site_select">Users</button>
        </div>
        <div class="dash_btn px-5">
          <input class="btn btn_site_select border-0" type="text" placeholder="Search User " /><i class="fa fa-search" style="margin-left: -2rem; color:white;"></i>
        </div>
        <div class="ms-auto all_selects d-flex">
          <select class="form-select btn_site_select border-0 form-select-sm" aria-label=".form-select-sm example">
            <option selected>Amplitude</option>
            <option value="1">High</option>
            <option value="2">Low</option>
          </select>

          <select class="form-select mx-2 btn_site_select border-0 form-select-sm" aria-label=".form-select-sm example">
            <option selected>Frequency</option>
            <option value="1">One</option>
            <option value="2">Two</option>
            <option value="3">Three</option>
          </select>

          <select class="form-select btn_site_select border-0 form-select-sm" aria-label=".form-select-sm example">
            <option selected>Time</option>
            <option value="1">Less than 1 Hr</option>
            <option value="1">1 Hr</option>
            <option value="2">2 Hr</option>
            <option value="3">3 Hr</option>
          </select>
        </div>
      </div>

      <div class="card table_card rounded-4 mt-4">
        <table class="table align-middle mb-0 border-white">
          <thead class="table_dark text-white">
            <tr>
              <th scope="col">User Name</th>
              <th scope="col">Amplitude</th>
              <th scope="col">Frequency</th>
              <th scope="col">Time</th>
            </tr>
          </thead>
          <tbody>
            <?php for ($i = 0; $i <= 6; $i++) { ?>
              <tr class="inner_tr" style="cursor: pointer;" onclick="window.location = './statistics.php'">
                <th scope="row">
                  <div class="d-flex align-items-center">
                    <img src="assets/images/profilePicMask.png" alt="userprofile" class="rounded-circle img-fluid">
                    <div class="d-flex ms-2 flex-column user_imp">
                      <h6 class="mb-0">Jason Statham</h6>
                      <span>Impression</span>
                    </div>
                  </div>
                </th>
                <td>
                  <div class="chart_div">
                    <div class="chart">
                      <img src="assets/images/Amplitude.png" class="img-fluid" alt="amp">
                    </div>
                    <p>HIGH</p>
                  </div>
                </td>
                <td>
                  <div class="chart_div hz_div">
                    <div class="chart">
                      <img src="assets/images/Frequency.png" class="img-fluid" alt="amp">
                    </div>
                    <p>5.6</p>
                    <span>Hz</span>
                  </div>
                </td>
                <td>
                  <div class="chart_div per_day">
                    <div class="chart">
                      <img src="assets/images/Time.png" class="img-fluid" alt="amp">
                    </div>
                    <p>2Hr 20min</p>
                    <span>Per Day</span>
                  </div>
                </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>

    </div>
  </div>


  <?php include 'include/bottom_links.php'; ?>
</body>

</html>