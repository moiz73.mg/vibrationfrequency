var menu_btn = document.querySelector("#menu-btn");
var sidebar = document.querySelector("#sidebar");
var container = document.querySelector(".my_container");
menu_btn.addEventListener("click", () => {
  sidebar.classList.toggle("active_nav");
  container.classList.toggle("active-cont");
});

$(document).ready(function () {
  $(".one").click(function () {
    $(this).addClass("active_side").siblings().removeClass("active_side");
  });

  $('.main_slider').slick({
    infinite: true,
    speed: 1000,
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow: '<div class="next_arrow"><i class="bi text-white bi-arrow-right-short"></i></div> ',
    prevArrow: '<div class="prev_arrow"><i class="bi text-white bi-arrow-left-short"></i></div> ',
  });

  $('.card_slider').slick({
    dots: true,
    speed: 1000,
    arrows: false,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });


});
