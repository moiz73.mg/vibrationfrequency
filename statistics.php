<!DOCTYPE html>
<html lang="en">

<head>
  <?php include 'include/top_links.php'; ?>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">
</head>

<body class="dash_body">


  <div class="header_content">
    <div class="container-fluid container-xxl">
      <div class="d-flex align-items-center pt-3">
        <div class="hard_vib">
          <i class="fa-solid fa-left-long text-white" onclick="window.location='./dashboard.php'"></i>
          <h1 class="text-capitalize">Hardware-vibration</h1>
        </div>
        <div class="dropdown_user ms-auto">
          <div class="dropdown">
            <a class="dropdown-toggle u_droptoggle text-white" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
              <img src="assets/images/profilePicMask.png" alt="userprofile" class="rounded-circle img-fluid">
              <span class="u_name">Elizabetg Kin</span>
            </a>

            <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
              <li><a class="dropdown-item" href="dashboard.php">Dashboard</a></li>
              <li><a class="dropdown-item" href="statistics.php">Statistics</a></li>
              <li><a class="dropdown-item" href="index.php">Logout</a></li>
            </ul>
          </div>
        </div>
      </div>

      <div class="d-flex justify-content-between">
        <div class="butns">
          <div class="dash_btn">
            <button class="btn btn_site_select">Statistics</button>
          </div>
        </div>
        <div class="butns">
          <div class="dash_btn">
            <button class="btn btn_site_select">Export to CSV</button>
          </div>
        </div>
      </div>


      <div class="line_chart p-3 my-4">
        <div class="d-flex align-items-center">
          <img src="assets/images/profilePicMask.png" alt="userprofile" class="rounded-circle img-fluid">
          <div class="d-flex ms-2 flex-column user_imp">
            <h6 class="mb-0">Jason Statham</h6>
            <span>Impression</span>
          </div>

          <div class="ms-auto">
            <div class="weeks_section d-flex align-items-center">
              <span>all time</span>
              <span>this year</span>
              <span>this week</span>
              <span>today</span>
              <div class="dropdown">
                <a class="dropdown-toggle dots_toggle text-secondary" href="#" role="button" id="dotsdropdown" data-bs-toggle="dropdown" aria-expanded="false">
                  <h4 class="mb-0"> <i class="bi bi-three-dots-vertical text-secondary"></i></h4>
                </a>

                <ul class="dropdown-menu" aria-labelledby="dotsdropdown">
                  <li><a class="dropdown-item" href="dashboard.php">Dashboard</a></li>
                  <li><a class="dropdown-item" href="statistics.php">Statistics</a></li>
                </ul>
              </div>
            </div>
          </div>

        </div>


        <figure class="highcharts-figure">
          <div id="container"></div>
        </figure>

      </div>

      <div class="card table_card rounded-4 mt-4">
        <table class="table align-middle mb-0 border-white">
          <thead class="table_dark text-white">
            <tr>
              <th scope="col">From</th>
              <th scope="col">To</th>
              <th scope="col">Amplitude</th>
              <th scope="col">Frequency</th>
            </tr>
          </thead>
          <tbody>
            <?php for ($i = 0; $i <= 6; $i++) { ?>
              <tr class="inner_tr">
                <th scope="row">
                  <div class="chart_div per_day">
                    <p>07:00 PM</p>
                    <span>Today</span>
                  </div>
                </th>
                <td>
                  <div class="chart_div per_day">
                    <p>09:00 PM</p>
                    <span>Today</span>
                  </div>
                </td>
                <td>
                  <div class="chart_div">
                    <p>HIGH</p>
                  </div>
                </td>
                <td>
                  <div class="chart_div hz_div">
                    <p>5.6</p>
                    <span>Hz</span>
                  </div>
                </td>

              </tr>
            <?php } ?>

          </tbody>
        </table>
      </div>

    </div>
  </div>








  <?php include 'include/bottom_links.php'; ?>

  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <script src="https://code.highcharts.com/modules/export-data.js"></script>
  <script src="https://code.highcharts.com/modules/accessibility.js"></script>


  <script>
    Highcharts.chart('container', {
      chart: {
        type: 'areaspline'
      },
      title: {
        text: 'Timeline of Activity 10/9/18 - 16/9/18',
        align: 'left',
        style: {
          color: '#8d9294',
        }
      },
      legend: {
        layout: 'horizontal',
        align: 'right',
        verticalAlign: 'top',
        x: 0,
        y: 0,
        floating: true,
        borderWidth: 1,
        backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF'
      },
      xAxis: {
        categories: [
          'Sep 10',
          'Sep 11',
          'Sep 12',
          'Sep 13',
          'Sep 14',
          'Sep 15',
          'Sep 16'
        ],
        plotBands: [{ // visualize the weekend
          from: 4.5,
          to: 6.5,
          color: 'rgba(68, 170, 213, .2)'
        }]
      },
      yAxis: {
        title: {
          text: ''
        },
        labels: {
          enabled: false
        },
      },
      tooltip: {
        shared: true,
        valueSuffix: ' units'
      },
      credits: {
        enabled: false
      },
      plotOptions: {
        areaspline: {
          fillOpacity: 0.5
        }
      },
      series: [{
        name: 'Amplitude',
        data: [3, 4, 3, 5, 4, 10, 12],
        color: '#3892a8',
      }, {
        name: 'Frequency',
        data: [1, 3, 4, 3, 3, 5, 4],
        color: '#1b92af',
      }, {
        name: 'Time',
        data: [1, 3, 5, 4, 4, 5, 8],
        color: '#a7153a',
      }]
    });
  </script>
</body>

</html>