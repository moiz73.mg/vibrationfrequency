<!DOCTYPE html>
<html lang="en">

<head>
  <?php include 'include/top_links.php'; ?>
</head>

<body>
<div class="main_bg">
<div class="container">
  <div class="row vh-100 justify-content-center align-items-center">
<div class="col-12 col-md-6 col-lg-4">
    <div class="login_section px-3 py-2">
      <form action="dashboard.php">
        <div class="text-center mb-5 hard_vib">
            <h1>Hardware  <br> vibration </h1>
        </div>
        <div class="sign_in">Sign in</div>

        <div class="form-group my-3">
            <fieldset class="the-fieldset rounded-3">
                <legend class="the-legend float-none">Username</legend>
                <input type="text" placeholder="John@gmail.com" class="form-control text-white p-0 ps-1 bg-transparent border-0">
            </fieldset>
        </div>
        <div class="form-group">
            <fieldset class="the-fieldset rounded-3">
                <legend class="the-legend float-none">Password</legend>
                <input type="password" placeholder="********" class="form-control text-white p-0 ps-1 bg-transparent border-0">
            </fieldset>
        </div>

        <div class="keep_forgot my-2 text-capitalize d-flex align-items-center">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                <label class="form-check-label text-white" for="flexCheckDefault">
                 Keep Me signed in
                </label>
              </div>
            <div class="ms-auto">
                <a href="#/" class="text-white">
                <span class="color_white_font">forgot password?</span>
            </a>
            </div>
        </div>

        <div class="sub text-center">
            <button class="btn btn_signsubmit">sign in</button>
            <span class="color_white_font">Don't have an account? <a href="signup.php" class="text-white">SIGN UP</a> </span>
        </div>

      </form>
    </div>
    </div>
  </div>
</div>
</div>



  <?php include 'include/bottom_links.php'; ?>
</body>

</html>